﻿using GameTiming.Models;
using GameTiming.Utils;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;

namespace GameTiming.Controllers
{
    public class ApiBaseController : ApiController
    {
        protected MainDB DB = new MainDB();
        protected string HostRootPath { get { return HostingEnvironment.MapPath("\\"); } }

        protected string ServerRootPath { get { return HostingEnvironment.MapPath("~"); } }

        protected void SaveOrReplaceFile(HttpPostedFile file, string filePath, string oldPath)
        {
            deleteCurrentFile(oldPath);
            WriteFile(file, filePath);
        }

        private void deleteCurrentFile(string currentFilePath)
        {
            if (!string.IsNullOrWhiteSpace(currentFilePath))
            {
                var fullName = Path.Combine(ServerRootPath, currentFilePath);
                if (File.Exists(fullName))
                    File.Delete(fullName);
            }
        }
        protected string ProduceFilePath(int Id, HttpPostedFile file, UploadedFileType fileType)
        {
            string correctExtension = Path.GetExtension(file.FileName).ToLower().Replace(".", "");
            string fileName = "Id" + Id.ToString() + "." + correctExtension;
            string mainDirectory = ConfigurationManager.AppSettings["UploadedFilesDirectory"];
            return Path.Combine(mainDirectory, fileType.ToString(), fileName);
        }
        private void WriteFile(HttpPostedFile file, string filePath)
        {
            string fileFullPath = Path.Combine(ServerRootPath, filePath);

            if (!Directory.Exists(Path.GetDirectoryName(fileFullPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(fileFullPath));
            file.SaveAs(fileFullPath);
        }

        protected void CheckFileExtensionIsValid(HttpPostedFile file, string validFormats)
        {
            var formats = "-" + validFormats + "-";
            var extension = "-" + Path.GetExtension(file.FileName).ToLower().Replace(".", "") + "-";

            if (!formats.Contains(extension))
                throw new ProcessException("فرمت تصویر مورد نظر مجاز نمی باشد.");
        }


        protected void CheckFileMaxSizeIsValid(HttpPostedFile file, int sizeInMB, string fielName)
        {
            if (file.ContentLength > (sizeInMB * 1024 * 1024))
                throw new ProcessException(string.Format("حجم {0} باید کمتر از {1} مگابایت باشد.", sizeInMB, fielName));
        }


        protected IHttpActionResult SendGood(object body)
        {
            return Ok(MakResponse.GetResult(body));
        }
        protected IHttpActionResult SendError(string message, ResultCode resultCode)
        {
            return Ok(MakResponse.GetError((int)resultCode, message));
        }
        protected IHttpActionResult SendError(string message)
        {
            return Ok(MakResponse.GetError(message));
        }

        protected string GetStringInForm(string fieldName, bool isRequired, string fieldPersianName)
        {
            NameValueCollection form = HttpContext.Current.Request.Form;
            string[] values = form.GetValues(fieldName);

            if (values == null || values.Length == 0 || string.IsNullOrWhiteSpace(values[0]))
            {
                if (isRequired)
                    throw new ProcessException(string.Format(Messages.Field_IsRequired, fieldPersianName));
                else
                    return null;
            }
            else
                return values[0].Clear();
        }

        protected int GetIntInForm(string fieldName, bool isRequired, string fieldPersianName)
        {
            string strValue = GetStringInForm(fieldName, isRequired, fieldPersianName);
            if(string.IsNullOrWhiteSpace(strValue) && !isRequired)
                return 0;
            else
            {
                int intVal = 0;
                bool isValid = int.TryParse(strValue , out intVal);
                if (!isValid)
                    throw new ProcessException(string.Format(Messages.Field_IsRequired, fieldPersianName));
                else
                    return intVal;
            }
        }

        protected void CheckGenderIsValid(int Gender)
        {
            if (Gender != 0 && Gender != 1)
                throw new ProcessException("جنسیت صحیح نمی باشد");
        }

        protected HttpPostedFile GetFileInForm(string fieldName, bool isRequired, string fieldPersianName)
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files["fieldName"] : null;

            if ((isRequired) && (file == null || file.ContentLength == 0))
                throw new ProcessException(string.Format(Messages.Field_IsRequired, fieldPersianName));
            else
                return file;
        }

        protected HttpPostedFile GetFirstFileInForm(bool isRequired, string fieldPersianName)
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

            if ((isRequired) && (file == null || file.ContentLength == 0))
                throw new ProcessException(string.Format(Messages.Field_IsRequired, fieldPersianName));
            else
                return file;
        }

        protected string GetVirtualPath(string FilePath)
        {
            if (string.IsNullOrWhiteSpace(FilePath))
                return "";

            var baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            var virtualFilePath = baseUrl + "/" + FilePath.Replace("\\", "/");
            return virtualFilePath;
        }
    }
}
