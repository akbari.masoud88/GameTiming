﻿using GameTiming.Models;
using System.Web.Mvc;
using System.Linq;

namespace GameTiming.Controllers
{
    public class HomeController : MVCBaseController
    {
        private MainDB db = new MainDB();
        // GET: Home
        public ActionResult Index()
        {
            var user = db.Users.ToList();

            return View();
        }
    }
}