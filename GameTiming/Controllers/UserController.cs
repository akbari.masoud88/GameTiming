﻿using GameTiming.Models;
using System.Web.Helpers;
using System.Web.Http;
using System.Linq;
using System.Web;
using GameTiming.Utils;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Web.Hosting;
using System.Configuration;

namespace GameTiming.Controllers
{
    public class UserController : ApiBaseController
    {
        [AcceptVerbs("Post")]
        public IHttpActionResult RegisterUser()
        {
            string fullName = GetStringInForm("fullName", true, "نام کامل");

            #region phoneNumbersValidation
            string phoneNum = GetStringInForm("phoneNumber", true, "شماره تلفن");
            phoneNum.CheckPhoneNumberIsValid();

            if (DB.Users.Any(c => c.PhoneNumber == phoneNum && c.Active))
                return SendError(Messages.PhoneNumberIsRepeated);
            #endregion

            var nextUserId = (DB.Users.Count() == 0) ? 1 : DB.Users.Max(c => c.Id) + 1;
            var user = new User()
            {
                Id = nextUserId,
                FullName = fullName,
                Active = true,
                PhoneNumber = phoneNum,
            };
            DB.Users.Add(user);
            DB.SaveChanges();

            return SendGood(new
            {
                Id = nextUserId,
                FullName = fullName,
                PhoneNumber = phoneNum
            });
        }

        [AcceptVerbs("Post")]
        public IHttpActionResult UpdateUser()
        {
            string fullName = GetStringInForm("fullName", true, "نام کامل");

            string phoneNum = GetStringInForm("phoneNumber", true, "شماره تلفن");
            phoneNum.CheckPhoneNumberIsValid();

            var user = DB.Users.SingleOrDefault(c => c.PhoneNumber == phoneNum && c.Active);
            if (user == null)
                throw new ProcessException(Messages.AccountIsNotExist);

            user.FullName = fullName;
            DB.SaveChanges();

            return SendGood(new
            {
                Id = user.Id,
                FullName = fullName,
                PhoneNumber = phoneNum
            });
        }

        [AcceptVerbs("Post")]
        public IHttpActionResult Login()
        {
            string phoneNumer = GetStringInForm("phoneNumber", true, "شماره تلفن");
            phoneNumer.CheckPhoneNumberIsValid();

            User user = DB.Users.FirstOrDefault(u => u.PhoneNumber == phoneNumer && u.Active);
            if (user == null)
                return SendError(Messages.PhoneNumberNotExists);
            else
                return SendGood(new { user.Id, user.FullName, user.PhoneNumber });
        }

        [AcceptVerbs("Post")]
        public IHttpActionResult RegisterChild()
        {
            string phoneNumber = GetStringInForm("phoneNumber", true, "شماره تلفن");
            phoneNumber.CheckPhoneNumberIsValid();

            string birthDate = GetStringInForm("birthDate", true, "تاریخ تولد");
            birthDate.CheckBirthDateIsValid();

            string childFullName = GetStringInForm("childFullName", true, "نام کامل فرزند");

            string childNationalCode = GetStringInForm("nationalCode", false, "کد ملی فرزند");
            if (!string.IsNullOrWhiteSpace(childNationalCode))
                childNationalCode.CheckNationalCodeIsValid();

            string GenderId = GetStringInForm("Gender", true, "جنسیت");
            CheckGenderIsValid(int.Parse(GenderId));

            var file = GetFirstFileInForm(false, "تصویر فرزند");
            if (file != null)
            {
                CheckFileMaxSizeIsValid(file, 3, "تصویر");
                CheckFileExtensionIsValid(file, "jpg-jpeg-png");
            }
            var Parent = DB.Users.SingleOrDefault(user => user.PhoneNumber == phoneNumber && user.Active);
            if (Parent == null)
                return SendError(Messages.AccountIsNotExist);

            int maxChildCount = int.Parse(ConfigurationManager.AppSettings["MaxChildrenCount"]);
            if (DB.Children.Count(c => c.ParentId == Parent.Id) >= maxChildCount)
                throw new ProcessException(string.Format(Messages.MaximunChildCountIs_, maxChildCount));

            if (DB.Children.Any(c => c.ParentId == Parent.Id && c.FullName == childFullName))
                throw new ProcessException(Messages.ChildNameIsRepeated);

            var nextChildId = (DB.Children.Count() == 0) ? 1 : DB.Children.Max(c => c.Id) + 1;

            Child child = new Child();
            child.Id = nextChildId;
            child.ParentId = Parent.Id;
            child.GenderId = int.Parse(GenderId);
            child.FullName = childFullName;
            child.BirthDate = birthDate.CastToInt();
            child.NationalCode = childNationalCode;

            if (file != null)
                child.ImagePath = ProduceFilePath(nextChildId, file, UploadedFileType.ChildProfile);

            DB.Children.Add(child);
            DB.SaveChanges();

            if (file != null)
                SaveOrReplaceFile(file, child.ImagePath, "");

            return SendGood(new
            {
                child.Id,
                child.ParentId,
                child.FullName,
                child.GenderId,
                BirthDate = child.BirthDateStr,
                child.NationalCode,
                ImagePath = GetVirtualPath(child.ImagePath)
            });
        }

        [AcceptVerbs("Post")]
        public IHttpActionResult UpdateChild()
        {
            string phoneNumber = GetStringInForm("phoneNumber", true, "شماره تلفن");
            phoneNumber.CheckPhoneNumberIsValid();

            string birthDate = GetStringInForm("birthDate", true, "تاریخ تولد");
            birthDate.CheckBirthDateIsValid();

            string childFullName = GetStringInForm("childFullName", true, "نام کامل فرزند");

            string childNationalCode = GetStringInForm("nationalCode", false, "کد ملی فرزند");
            if (!string.IsNullOrWhiteSpace(childNationalCode))
                childNationalCode.CheckNationalCodeIsValid();

            string GenderId = GetStringInForm("Gender", true, "جنسیت");
            CheckGenderIsValid(int.Parse(GenderId));

            var file = GetFirstFileInForm(false, "تصویر فرزند");
            if (file != null)
            {
                CheckFileMaxSizeIsValid(file, 3, "تصویر");
                CheckFileExtensionIsValid(file, "jpg-jpeg-png");
            }
            var Parent = DB.Users.SingleOrDefault(user => user.PhoneNumber == phoneNumber && user.Active);
            if (Parent == null)
                return SendError(Messages.AccountIsNotExist);

            int childId = GetIntInForm("childId", true, "کد فرزند");

            Child child = DB.Children.SingleOrDefault(c => c.Id == childId && c.ParentId == Parent.Id);
            if (child == null)
                throw new ProcessException(Messages.RecordNotExist);

            if (Parent.Children.Any(c => c.FullName == childFullName && c.Id != childId))
                throw new ProcessException(Messages.ChildNameIsRepeated);

            child.GenderId = int.Parse(GenderId);
            child.FullName = childFullName;
            child.BirthDate = birthDate.CastToInt();
            child.NationalCode = childNationalCode;

            string oldImagePath = child.ImagePath;
            if (file != null)
                child.ImagePath = ProduceFilePath(childId, file, UploadedFileType.ChildProfile);

            DB.SaveChanges();

            if (file != null)
                SaveOrReplaceFile(file, child.ImagePath, oldImagePath);

            return SendGood(new
            {
                child.Id,
                child.ParentId,
                child.FullName,
                child.GenderId,
                BirthDate = child.BirthDateStr,
                child.NationalCode,
                ImagePath = GetVirtualPath(child.ImagePath)
            });
        }
        [AcceptVerbs("Post")]
        public IHttpActionResult GetAllChilds()
        {
            int userId = GetIntInForm("userId", true, "کد کاربری");
            //ابتدا از جدول فرزندان , کل فرزندان کاربر مورد نظر را انتخاب می کنیم-خط زیر
            List<Child> childrenList = DB.Children.Where(c => c.ParentId == userId).ToList();

            //ما نباید کل اطلاعاتی که از دیتابیس گرفته ایم را به کاربر نشان دهیم,
            //چون ممکن است در جدول فرزندان چندین کلید خارجی وجود داشته باشد
            //و ما نباید اطلاعات کلیدهای خارجی را به سمت کاربر بفرستیم
            //یا مثلا در یک جدول فرضی ممکن است کلمه عبور وجود داشته باشد. نباید این اطلاعات به کاربر فرستاده شود
            //پس باید از اطلاعاتی که از دیتابیس گرفته ایم، فقط فیلدهای مفید و کاربردی را به سمت کاربر بفرستیم
            //برای اینکار یا باید یک لیست نیو کنیم و آنرا با فورایچ پرکنیم! یا از روش کوتاهتر زیر -روش لینک- استفاده کنیم
            var dataForClient = childrenList.Select(child => new
                                                    {
                                                        child.Id,
                                                        child.ParentId,
                                                        child.FullName,
                                                        child.GenderId,
                                                        BirthDate = child.BirthDateStr,
                                                        child.NationalCode,
                                                        ImagePath = GetVirtualPath(child.ImagePath)
                                                    });
            return SendGood(dataForClient);
        }

        [AcceptVerbs("Post")]
        public IHttpActionResult UploadChildProfile()
        {
            int ChildId = GetIntInForm("ChildId", false, null);

            var file = GetFirstFileInForm(true, "تصویر");
            CheckFileExtensionIsValid(file, "jpg-jpeg-png");
            CheckFileMaxSizeIsValid(file, 3, "تصویر");

            var child = DB.Children.FirstOrDefault(c => c.Id == ChildId);
            if (child == null)
                throw new Exception("child Id Is Not Valid");

            string path = ProduceFilePath(child.Id, file, UploadedFileType.ChildProfile);
            string currentPath = child.ImagePath;
            child.ImagePath = path;
            DB.SaveChanges();

            SaveOrReplaceFile(file, path, "");
            return SendGood(new { child.Id, child.ParentId, child.FullName, child.GenderId, BirthDate = child.BirthDateStr, ImagePath = GetVirtualPath(child.ImagePath) });
        }

        ////////////////////////////
        //[AcceptVerbs("Post")]
        //public IHttpActionResult RegisterPlayer(int childFullName, string phoneNumber)
        //{
        //    var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

        //    if (file != null)
        //    {
        //        CheckFileMaxSizeIsValid(file, 3);
        //        CheckFileExtensionIsValid(file, "jpg-jpeg-png");
        //    }
        //    var Parent = DB.Users.FirstOrDefault(user => user.PhoneNumber == phoneNumber && user.Active);
        //    if (Parent == null)
        //        return SendError(Messages.AccountIsNotExist);

        //    var nextChildId = (DB.Children.Count() == 0) ? 1 : DB.Users.Max(c => c.Id) + 1;

        //    Child child = new Child();
        //    child.Id = nextChildId;
        //    child.ParentId = Parent.Id;
        //    child.GenderId = (int)GenderEnum.Female;

        //    DB.Children.Add(child);
        //    DB.SaveChanges();

        //    if (file != null)
        //    {
        //        string path = ProduceFilePath(nextChildId, file, UploadedFileType.ChildProfile);
        //        SaveOrReplaceFile(file, path, "");
        //    }
        //    return SendGood(new { child.Id, });
        //}
        [AcceptVerbs("Post")]
        public IHttpActionResult GetAllUsers()
        {
            List<User> userList = DB.Users.ToList();
            return SendGood(userList.Select(user => new
            {
                user.Id,
                user.PhoneNumber,
                user.FullName,
            })
            );
        }
    }
}
