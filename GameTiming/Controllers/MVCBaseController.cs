﻿using GameTiming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace GameTiming.Controllers
{
    public class MVCBaseController : Controller
    {
        protected MainDB DB = new MainDB();
        protected string ServerRootPath { get { return Server.MapPath("\\"); } }

        //protected void CheckFileExtensionIsValid(HttpPostedFileBase file, string validFormats)
        //{
        //    var formats = "-" + validFormats + "-";
        //    var extension = "-" + Path.GetExtension(file.FileName).ToLower().Replace(".", "") + "-";

        //    if (!formats.Contains(extension))
        //        throw new MakException("فقط فایل با فرمت مجاز قابل قبول میباشد: " + validFormats);
        //}
        //protected void CheckFileExtensionIsValid(string extension, string validFormats)
        //{
        //    var formats = "-" + validFormats + "-";
        //    extension = "-" + extension.ToLower().Replace(".", "") + "-";

        //    if (!formats.Contains(extension))
        //        throw new MakException("فقط فایل با فرمت مجاز قابل قبول میباشد: " + validFormats);
        //}
        //protected void CheckFileMaxSizeIsValid(HttpPostedFileBase file, int sizeInMB)
        //{
        //    if (file.ContentLength > (sizeInMB * 1024 * 1024))
        //        throw new MakException(string.Format("حجم فایل باید کمتر از {0} مگابایت باشد.", sizeInMB));
        //}

        //protected void CheckFilesCountIsValid(IEnumerable<HttpPostedFileBase> files, int maxCount)
        //{
        //    if (files != null && (files.Count() > maxCount))
        //        throw new MakException(string.Format("تعداد فایل ها باید کمتر از {0}  باشد.", maxCount));
        //}
    }
}
