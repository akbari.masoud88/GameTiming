﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace GameTiming.Models
{

    public class MainDB : DbContext
    {
        static MainDB()
        {
            Database.SetInitializer<MainDB>(new DBInitializer());
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MainDB, Migrations.Configuration>());
        }
        public MainDB() : base("name=GameTiming")
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Child> Children { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Entertainment> Entertainments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Player>()
                .HasRequired(x => x.User)
                .WithMany(x => x.Players)
                .HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Child>()
                .HasRequired(x => x.Parent)
                .WithMany(x => x.Children)
                .HasForeignKey(x => x.ParentId);


            modelBuilder.Entity<User>().HasIndex(c => c.PhoneNumber).IsUnique(true);
        }
    }
}