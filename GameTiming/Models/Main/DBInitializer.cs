﻿using GameTiming.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Helpers;

namespace GameTiming.Models
{
    public class DBInitializer : DropCreateDatabaseIfModelChanges<MainDB>
    {
        protected override void Seed(MainDB context)
        {
            base.Seed(context);

            //context.Database.ExecuteSqlCommand("CREATE USER GameTiming FOR LOGIN GameTiming; ");

            Entertainment e1 = new Entertainment();
            e1.Id = 1;
            e1.Active = true;
            e1.Name = "مجتمع تفریحی 1 ";
            e1.Address = "آدرس 1 ";

            e1.Tickets.Add(new Ticket()
                {
                    Id = 1,
                    Active = true,
                    FromHour = 16,
                    ToHour = 18,
                    Price = (decimal)120000.00,
                    PersianDate = 13970510,
                    GenderId = (int)GenderEnum.Male,
                    Capacity = 100,
                });

            e1.Tickets.Add(new Ticket()
            {
                Id = 2,
                Active = true,
                FromHour = 18,
                ToHour = 20,
                Price = (decimal)120000.00,
                PersianDate = 13970510,
                GenderId = (int)GenderEnum.Male,
                Capacity = 50,
            });
            context.Entertainments.Add(e1);


            string password = "123456";
            var salt = Crypto.GenerateSalt();
            var HashedPass = Crypto.HashPassword(salt + password);
            User user1 = new User()
            {
                Id = 1,
                PhoneNumber = "09122888888",
                FullName = "user1",
                Active = true
            };
            user1.Children.Add(new Child()
            {
                Id = 1,
                GenderId = (int)GenderEnum.Female,
                FullName = "child1",
                BirthDate = 13920202,
                
            });
            user1.Children.Add(new Child()
            {
                Id = 2,
                GenderId = (int)GenderEnum.Male,
                FullName = "child2",
                BirthDate = 13940202,
            });
            context.Users.Add(user1);
            context.SaveChanges();

        }
    }
}