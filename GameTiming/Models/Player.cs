﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;


namespace GameTiming.Models
{
    public class Player : BaseModel
    {
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        [Required]
        public int GenderId { get; set; }
        public int BirthDate { get; set; }

        public string ProfilePath { get; set; }

        [Required]
        public int TicketId { get; set; }
        [Required]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}