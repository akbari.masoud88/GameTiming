﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GameTiming.Models
{
    public class Ticket : BaseModel
    {
        public Ticket()
        {
            this.Players = new HashSet<Player>();
        }
        [Required]
        public decimal Price { set; get; }

        [Required]
        public int GenderId { get; set; }
        
        [Required]
        public int PersianDate { get; set; }
        
        [Required]
        public int FromHour { get; set; }
        
        [Required]
        public int ToHour { get; set; }

        [Required]
        public int Capacity { get; set; }

        public bool Active { get; set; }

        [MaxLength(2000)]
        public string Description { get; set; }

        [NotMapped]
        public string Time { get { return FromHour.ToString() + " تا " + ToHour.ToString(); } }

        public virtual ICollection<Player> Players { get; set; }

        public int EntertainmentId { get; set; }
        public virtual Entertainment Entertainment  { get; set; }


    }
}