﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameTiming.Models
{
    [System.ComponentModel.DataAnnotations.Schema.Table("Users")]
    public class User : BaseModel
    {
        public User()
        {
            this.Children = new HashSet<Child>();
            this.Players = new HashSet<Player>();
        }

        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        [Required]
        [MaxLength(11)]
        public string PhoneNumber { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<Child> Children { get; set; }
        public virtual ICollection<Player> Players { get; set; }
    }


    public class UserModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
    }
}