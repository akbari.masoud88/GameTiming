﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using GameTiming.Utils;

namespace GameTiming.Models
{
    public class Child : BaseModel
    {
        [MaxLength(100)]
        [Required]
        public string FullName { get; set; }

        [MaxLength(10)]
        public string NationalCode { get; set; }

        public int BirthDate { get; set; }
        
        [Required]
        public int GenderId { get; set; } 

        [Required]
        public int ParentId { get; set; }

        [NotMapped]
        public string BirthDateStr { get { return BirthDate == 0 ? null : BirthDate.CastToPDate(); } }


        public string ImagePath { get; set; }

        public virtual User Parent { get; set; }


    }
}