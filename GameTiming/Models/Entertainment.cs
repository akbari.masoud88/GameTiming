﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameTiming.Models
{
    public class Entertainment : BaseModel
    {
        public Entertainment()
        {
            this.Tickets = new HashSet<Ticket>();
        }
        [MaxLength(50)]
        [Required]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Address { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}