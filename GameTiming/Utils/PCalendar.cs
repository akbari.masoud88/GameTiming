﻿using GameTiming.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GameTiming.Utils
{
    public class PCalendar
    {
        public static string CastDateTimeToPDateStr(DateTime datetime)
        {
            PersianCalendar pc = new PersianCalendar();
            string y = pc.GetYear(datetime).ToString();
            string m = pc.GetMonth(datetime).ToString().PadLeft(2, '0');
            string d = pc.GetDayOfMonth(datetime).ToString().PadLeft(2, '0');
            string nowdate = string.Format("{0}/{1}/{2}", y, m, d);
            return nowdate;
        }
        public static int CastDateTimeToPDateInt(DateTime datetime)
        {
            return int.Parse(CastDateTimeToPDateStr(datetime).Replace("/", ""));
        }
        public static string GetCurrentPersianDateStr()
        {
            return CastDateTimeToPDateStr(DateTime.Now);
        }
        public static string CurrentPersianDateStr { get { return GetCurrentPersianDateStr(); } }
        public static int GetCurrentPersianDateInt()
        {
            int nowdate = int.Parse(CurrentPersianDateStr.Replace("/", ""));
            return nowdate;
        }
        public static int CurrentPersianDateInt { get { return GetCurrentPersianDateInt(); } }

        public static DateTime CastPDateToDateTime(string PersianDate)
        {
            int year = PersianDate.Substring(0, 4).CastToInt();
            int month = PersianDate.Substring(5, 2).CastToInt();
            int day = PersianDate.Substring(8, 2).CastToInt();

            PersianCalendar pc = new PersianCalendar();
            DateTime date = pc.ToDateTime(year, month, day, 0, 0, 0, 0);
            return date;
        }
        public static string AddDays(string PersianDate, int Days)
        {
            DateTime tomorrow = CastPDateToDateTime(PersianDate).AddDays(Days);
            return CastDateTimeToPDateStr(tomorrow);
        }

        public static string CastToPDate(int PersianDate)
        {
            string strNDate = PersianDate.ToString();
            if (strNDate.Length != 8)
                throw new Exception(Messages.ForConvertNumberToPDateDigitsCountMustBe8);
            return strNDate.Insert(4, "/").Insert(7, "/");
        }
        public static int CastToInt(string PersianDate)
        {
            return int.Parse(PersianDate.Replace("/", ""));
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public static string CurrentPDayOfWeek { get { return GetDayOfWeek(); } }
        public static string GetDayOfWeek()
        {
            PersianCalendar pc = new PersianCalendar();
            var nowday = pc.GetDayOfWeek(DateTime.Now);
            string PDayOfWeek = string.Empty;

            switch (nowday)
            {
                case DayOfWeek.Friday:
                    PDayOfWeek = "جمعه";
                    break;

                case DayOfWeek.Monday:
                    PDayOfWeek = "دو شنبه";
                    break;

                case DayOfWeek.Saturday:
                    PDayOfWeek = "شنبه";
                    break;

                case DayOfWeek.Sunday:
                    PDayOfWeek = "یک شنبه";
                    break;

                case DayOfWeek.Thursday:
                    PDayOfWeek = "پنج شنبه";
                    break;

                case DayOfWeek.Tuesday:
                    PDayOfWeek = "سه شنبه";
                    break;

                case DayOfWeek.Wednesday:
                    PDayOfWeek = "چهار شنبه";
                    break;
            }
            return PDayOfWeek;
        }
        public static string GetCurrentTime()
        {
            string time = DateTime.Now.Hour.ToString().PadLeft(2, '0') + " : " +
                DateTime.Now.Minute.ToString().PadLeft(2, '0') + " : " +
                DateTime.Now.Second.ToString().PadLeft(2, '0');
            return time;
        }
        public static bool IsCurrentPersianDate(string PersianDate)
        {
            if (PersianDate == PCalendar.GetCurrentPersianDateStr())
                return true;
            else
                return false;
        }
        public static bool IsCurrentPersianDate(int PersianDate)
        {
            if (PersianDate == PCalendar.GetCurrentPersianDateInt())
                return true;
            else
                return false;
        }
        public static bool IsGreaterOrEqual(string SrcDate, string CompareDate)
        {
            if (int.Parse(SrcDate.Replace("/", "")) >= int.Parse(CompareDate.Replace("/", "")))
                return true;
            else
                return false;
        }
        public static bool IsLowerOrEqual(string SrcDate, string CompareDate)
        {
            if (int.Parse(SrcDate.Replace("/", "")) <= int.Parse(CompareDate.Replace("/", "")))
                return true;
            else
                return false;
        }

        public static bool IsGreater(string SrcDate, string CompareDate)
        {
            if (int.Parse(SrcDate.Replace("/", "")) > int.Parse(CompareDate.Replace("/", "")))
                return true;
            else
                return false;
        }
        public static bool IsLower(string SrcDate, string CompareDate)
        {
            if (int.Parse(SrcDate.Replace("/", "")) < int.Parse(CompareDate.Replace("/", "")))
                return true;
            else
                return false;
        }
        public static bool HasValidDateFormat(string PersianDate)
        {
            string PDate = PersianDate.Trim();
            if (PDate.Length != 10)
                return false;

            if (PDate.IndexOf("/") != 4)
                return false;

            if (PDate.LastIndexOf("/") != 7)
                return false;

            string PDate8Char = PDate.Replace("/", "");

            if (PDate8Char.Length != 8)
                return false;

            int intPDate = 0;
            if (!int.TryParse(PDate8Char, out intPDate))
                return false;

            string strmonth = PDate8Char.Substring(4, 2);
            int intmonth = 0;
            if (!int.TryParse(strmonth, out intmonth))
                return false;

            if (intmonth < 1 || intmonth > 12)
                return false;

            string strday = PDate8Char.Substring(6, 2);
            int intday = 0;
            if (!int.TryParse(strday, out intday))
                return false;

            if (intday < 1)
                return false;

            if (intmonth > 6 && intday > 30)
                return false;

            if (intmonth <= 6 && intday > 31)
                return false;

            return true;
        }

    }
}