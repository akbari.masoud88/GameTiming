﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameTiming.Utils
{
    public static class ExtensionMethods
    {
        public static int CastToInt(this string PersianDate)
        {
            return int.Parse(PersianDate.Replace("/", ""));
        }

        public static string CastToPDate(this int PersianDate)
        {
            string strNDate = PersianDate.ToString();
            if (strNDate.Length != 8)
                throw new Exception(Messages.ForConvertNumberToPDateDigitsCountMustBe8);
            return strNDate.Insert(4, "/").Insert(7, "/");
        }

        public static string Clear(this string Str)
        {
            return Str.Trim().Replace('۰', '0').Replace('۱', '1').Replace('۲', '2').Replace('۳', '3').Replace('۴', '4')
                        .Replace('۵', '5').Replace('۶', '6').Replace('۷', '7').Replace('۸', '8').Replace('۹', '9');

        }

        public static bool IsDigitsOnly(this string str)
        {
            foreach (char c in str.Clear())
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        public static void CheckPhoneNumberIsValid(this string phoneNumber)
        {
            if (phoneNumber.Length != 11)
                throw new ProcessException("شماره تلفن باید 11 رقم باشد.");

            if (!phoneNumber.StartsWith("09") || !phoneNumber.IsDigitsOnly())
                throw new ProcessException("شماره تلفن وارد شده صحیح نمی باشد.");
        }
        public static void CheckNationalCodeIsValid(this string nationalCode)
        {
            if (nationalCode.Length != 10)
                throw new ProcessException("کد ملی باید 10 رقم باشد.");
            if (!nationalCode.IsDigitsOnly())
                throw new ProcessException("کد ملی وارد شده صحیح نمی باشد.");
        }

        public static void CheckBirthDateIsValid(this string bitehDate)
        {
            if(!PCalendar.HasValidDateFormat(bitehDate))
                throw new ProcessException("تاریخ تولد وارد شده صحیح نمی باشد.");

        }
    }
}