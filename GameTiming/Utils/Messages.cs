﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameTiming.Utils
{
    public class Messages
    {
        public static readonly string ForConvertNumberToPDateDigitsCountMustBe8 = "عدد برای تبدیل به تاریخ شمسی باید 8 رقم داشته باشد.";
        public static readonly string ForCastStingCanNotBeNull = "برای تبدیل به عدد، نوع داده متنی نمی تواند Null باشد. ";
        public static readonly string Field_IsRequired = " {0} اجباری می باشد.";
        public static readonly string RecordNotExist = "رکورد مورد نظر وجود ندارد.";

        public static readonly string PhoneNumberIsRepeated = "شماره تلفن تکراری می باشد.";
        public static readonly string PhoneNumberNotExists = "شماره تلفن مورد نظر موجود نمی باشد.";

        public static readonly string AccountIsNotExist = "حساب کاربری مورد نظر وجود ندارد یا غیر فعال شده است..";
        public static readonly string PictureHasInvalidFormat = "فرمت تصویر مورد نظر مجاز نمی باشد.";

        public static readonly string MaximunChildCountIs_ = "حد اکثر تعداد فرزند قابل ثبت برای هر کاربر {0} نفر می باشد.";
        public static readonly string ChildNameIsRepeated = "نام فرزند تکراری می باشد.";

        
    }
}