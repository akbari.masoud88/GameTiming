﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace GameTiming.Utils
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public CustomExceptionFilter()
        {
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is ProcessException)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.SeeOther)
                {
                    Content = new StringContent(context.Exception.Message),
                };
                context.Response = response;
            }
            else
            {
                //todo
                // log in db  => ProcessException.GetTotalInnerExceptionsMessage(context.Exception)
            }
        }
    }


    public class ProcessException : Exception
    {
        public ProcessException(string message)
            : base(message)
        {
        }
        public static string GetTotalInnerExceptionsMessage(Exception ex)
        {
            string message = string.Empty;
            if (ex != null && ex.Message != null)
                message += "*$*" + ex.Message + Environment.NewLine;
            if (ex.InnerException != null && ex.InnerException.Message != null)
                message += GetTotalInnerExceptionsMessage(ex.InnerException);
            return message;
        }
    }
}