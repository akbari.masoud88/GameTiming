﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameTiming.Utils
{
    public enum GenderEnum
    {
        Male = 0,
        Female = 1,
        Both = 2,
    }

    public enum UploadedFileType
    {
        UserProfile = 1,
        ChildProfile = 2,
    }

 
    public enum ResultCode
    {
        //OK = 1,
        PhoneNumberIsRepeated = 2, //intent to login page
    }
}