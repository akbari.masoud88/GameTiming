﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace GameTiming.Utils
{
    public class MakResponse
    {
        public int ResultCode { get; set; }
        public string Message { get; set; }
        public object Body { get; set; }

        public static MakResponse GetError(int resultCode, string message)
        {
            return new MakResponse()
            {
                ResultCode = resultCode,
                Message = message,
                Body = null
            };
        }
        public static MakResponse GetError(string message)
        {
            return GetError(-1, message);
        }
        public static MakResponse GetResult(object body)
        {
            return new MakResponse()
            {
                Body = body,
                ResultCode = 1,
                Message = "",
            };
        }
    }
}