USE [master]
GO
/****** Object:  Database [GameTiming]    Script Date: 7/16/2020 1:38:50 PM ******/
CREATE DATABASE [GameTiming]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GameTiming', FILENAME = N'C:\Program Files (x86)\Plesk\Databases\MSSQL\MSSQL13.MSSQLSERVER2016\MSSQL\DATA\GameTiming.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'GameTiming_log', FILENAME = N'C:\Program Files (x86)\Plesk\Databases\MSSQL\MSSQL13.MSSQLSERVER2016\MSSQL\DATA\GameTiming_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GameTiming].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GameTiming] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GameTiming] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GameTiming] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GameTiming] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GameTiming] SET ARITHABORT OFF 
GO
ALTER DATABASE [GameTiming] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [GameTiming] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GameTiming] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GameTiming] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GameTiming] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GameTiming] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GameTiming] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GameTiming] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GameTiming] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GameTiming] SET  ENABLE_BROKER 
GO
ALTER DATABASE [GameTiming] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GameTiming] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GameTiming] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GameTiming] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GameTiming] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GameTiming] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GameTiming] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GameTiming] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [GameTiming] SET  MULTI_USER 
GO
ALTER DATABASE [GameTiming] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GameTiming] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GameTiming] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GameTiming] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [GameTiming] SET DELAYED_DURABILITY = DISABLED 
GO
USE [GameTiming]
GO
/****** Object:  User [GameTiming]    Script Date: 7/16/2020 1:38:52 PM ******/
CREATE USER [GameTiming] FOR LOGIN [GameTiming] WITH DEFAULT_SCHEMA=[GameTiming]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [GameTiming]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [GameTiming]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [GameTiming]
GO
ALTER ROLE [db_datareader] ADD MEMBER [GameTiming]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [GameTiming]
GO
/****** Object:  Schema [GameTiming]    Script Date: 7/16/2020 1:38:53 PM ******/
CREATE SCHEMA [GameTiming]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 7/16/2020 1:38:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Children]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Children](
	[Id] [int] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[NationalCode] [nvarchar](max) NULL,
	[BirthDate] [int] NOT NULL,
	[GenderId] [int] NOT NULL,
	[Relation] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Children] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Discounts]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Discounts](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](8) NOT NULL,
	[UserId] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Percent] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Discounts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Entertainments]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entertainments](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ShortAddress] [nvarchar](50) NULL,
	[Address] [nvarchar](300) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Entertainments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Games]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[Id] [int] NOT NULL,
	[GameName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Games] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GameTickets]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameTickets](
	[Id] [int] NOT NULL,
	[TicketId] [int] NOT NULL,
	[GameId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.GameTickets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Messages]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Messages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Percentages]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Percentages](
	[Id] [int] NOT NULL,
	[Count] [int] NOT NULL,
	[Percent] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Percentages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pictures]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pictures](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Path] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Pictures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Players]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Players](
	[Id] [int] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[GenderId] [int] NOT NULL,
	[Age] [int] NOT NULL,
	[ProfilePath] [nvarchar](max) NULL,
	[TicketId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Players] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Schools]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schools](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Path] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Schools] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shops]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shops](
	[Id] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[TicketId] [int] NOT NULL,
	[Date] [int] NOT NULL,
	[Pay] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Shops] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[Id] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[GenderId] [int] NOT NULL,
	[PersianDate] [int] NOT NULL,
	[FromHour] [int] NOT NULL,
	[ToHour] [int] NOT NULL,
	[FromAge] [int] NOT NULL,
	[ToAge] [int] NOT NULL,
	[Capacity] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Description] [nvarchar](2000) NULL,
	[EntertainmentId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Tickets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserMessages]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserMessages](
	[Id] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[MessageId] [int] NOT NULL,
	[Read] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.UserMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](11) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Videos]    Script Date: 7/16/2020 1:38:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Videos](
	[Id] [int] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Path] [nvarchar](max) NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Videos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_ParentId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_ParentId] ON [dbo].[Children]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Discounts]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_GameId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_GameId] ON [dbo].[GameTickets]
(
	[GameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TicketId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_TicketId] ON [dbo].[GameTickets]
(
	[TicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TicketId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_TicketId] ON [dbo].[Players]
(
	[TicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Players]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TicketId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_TicketId] ON [dbo].[Shops]
(
	[TicketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[Shops]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EntertainmentId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_EntertainmentId] ON [dbo].[Tickets]
(
	[EntertainmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MessageId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_MessageId] ON [dbo].[UserMessages]
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UserId]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[UserMessages]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_PhoneNumber]    Script Date: 7/16/2020 1:38:54 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_PhoneNumber] ON [dbo].[Users]
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Children]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Children_dbo.Users_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Children] CHECK CONSTRAINT [FK_dbo.Children_dbo.Users_ParentId]
GO
ALTER TABLE [dbo].[Discounts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Discounts_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Discounts] CHECK CONSTRAINT [FK_dbo.Discounts_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[GameTickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.GameTickets_dbo.Games_GameId] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([Id])
GO
ALTER TABLE [dbo].[GameTickets] CHECK CONSTRAINT [FK_dbo.GameTickets_dbo.Games_GameId]
GO
ALTER TABLE [dbo].[GameTickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.GameTickets_dbo.Tickets_TicketId] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Tickets] ([Id])
GO
ALTER TABLE [dbo].[GameTickets] CHECK CONSTRAINT [FK_dbo.GameTickets_dbo.Tickets_TicketId]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Players_dbo.Tickets_TicketId] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Tickets] ([Id])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_dbo.Players_dbo.Tickets_TicketId]
GO
ALTER TABLE [dbo].[Players]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Players_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Players] CHECK CONSTRAINT [FK_dbo.Players_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[Shops]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Shops_dbo.Tickets_TicketId] FOREIGN KEY([TicketId])
REFERENCES [dbo].[Tickets] ([Id])
GO
ALTER TABLE [dbo].[Shops] CHECK CONSTRAINT [FK_dbo.Shops_dbo.Tickets_TicketId]
GO
ALTER TABLE [dbo].[Shops]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Shops_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Shops] CHECK CONSTRAINT [FK_dbo.Shops_dbo.Users_UserId]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.Entertainments_EntertainmentId] FOREIGN KEY([EntertainmentId])
REFERENCES [dbo].[Entertainments] ([Id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.Entertainments_EntertainmentId]
GO
ALTER TABLE [dbo].[UserMessages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserMessages_dbo.Messages_MessageId] FOREIGN KEY([MessageId])
REFERENCES [dbo].[Messages] ([Id])
GO
ALTER TABLE [dbo].[UserMessages] CHECK CONSTRAINT [FK_dbo.UserMessages_dbo.Messages_MessageId]
GO
ALTER TABLE [dbo].[UserMessages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserMessages_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserMessages] CHECK CONSTRAINT [FK_dbo.UserMessages_dbo.Users_UserId]
GO
USE [master]
GO
ALTER DATABASE [GameTiming] SET  READ_WRITE 
GO
